# Prowler CI

Prowler for using inside CI or local user machines.

https://gitlab.com/helecloud/containers/prowler-ci

## Run locally

```bash
$~   docker run --rm -it registry.gitlab.com/helecloud/containers/prowler-ci:latest /bin/ash
```

## Run inside GitLab CI

```yaml
prowler:
  stage: secure
  image: registry.gitlab.com/helecloud/containers/prowler-ci:latest
  script:
    - /prowler/prowler -c check41
    - /prowler/prowler -c check41 -M mono > checks/prowler.txt
  allow_failure: true # you may want this if it is just to check output
  artifacts:
    paths:
      - checks/prowler-pre.txt
```

## Maintainer

* Will Hall